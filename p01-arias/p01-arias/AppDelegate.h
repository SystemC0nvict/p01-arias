//
//  AppDelegate.h
//  p01-arias
//
//  Created by Neo SX on 1/23/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

