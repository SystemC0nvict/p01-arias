//
//  main.m
//  p01-arias
//
//  Created by Neo SX on 1/23/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
