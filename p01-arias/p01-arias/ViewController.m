//
//  ViewController.m
//  p01-arias
//
//  Created by Neo SX on 1/23/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//I used code from this site the image help
//beageek.biz/how-to-set-background-image-uiview/
//and the image is form here
/*https://lh6.googleusercontent.com/-8ATNbVFy2ms/AAAAAAAAAAI/AAAAAAAAAC0/z7TTNtWZ1R8/s0-c-k-no-ns/photo.jpg
 */
//I tried to do an audio file but couldnt get it working so I did what I
//could for the image
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize wods;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) changeText
{
    if(_count == 0){
        [wods setText:@"Congrats, you pressed the button. What now?"];
        self.view.backgroundColor = [UIColor cyanColor];
    }
    else if(_count == 1){
        [wods setText:@"Congrats on pressing the button again.... "];
    }
    else if(_count == 2){
        [wods setText:@"Bored yet..."];
        self.view.backgroundColor = [UIColor greenColor];
    }
    else if(_count  == 3){
        [wods setText:@"Things won't go well if this keeps up."];
        self.view.backgroundColor = [UIColor orangeColor];
    }
    else if(_count  == 4){
        [wods setText:@"You should really stop."];
    }
    else if(_count  == 5){
        [wods setText:@"There is almost no going back"];
        self.view.backgroundColor = [UIColor redColor];
    }
    else if(_count  == 6){
        [wods setText:@"You are about to lose your eyes. Are you prepared?"];
        self.view.backgroundColor = [UIColor yellowColor];    }
    else if(_count  == 7){
        [wods setText:@"This is what you deserve."];
    }
    else {
        self.view.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"charlie.jpg"]];
        [self.view addSubview:backgroundView];    }
    _count += 1;
}


@end
